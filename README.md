# Technical Test #
**Version** 1.0.0  
**Author** Miles Thorp  
The aim of the test is to produce a mini application capable of loading, modifying and saving data using the pre designed Photoshop files. You should use best practices where possible and separate your presentation and logic. The application should be setup to use MVC where appropriate.

http://en.wikipedia.org/wiki/Model-view-controller

## Step 1 - PSD to HTML  ##
Update* ./site/index.php* and *./site/update.php* to match the design from the relevant pre designed Photoshop files. All icons are taken from Font Awesome (which has been included).

*./site/assets/less/styles.less* has been setup to incorporate the bootstrap grid system. Any additional styles should be added to *styles.less* or another file and imported. The less file should be compiled, minified and saved to* ./site/assets/css/styles.min.css*. This file has already been included in index.php and update.php.

Where possible you should save blocks of markup out as partials and re-use between pages to save on repeated code.

http://fortawesome.github.io/Font-Awesome/  
http://lesscss.org/

## Step 2 - User Login, Logout and Update  ##
When a user submits there details on the login form a check should be done against the database. Adequate validation should be performed to check that the user matches the user in the database. If the user doesn't exist return an error to the view and display to the user. If a user does exist save the customer to a session and redirect the customer to the update.php page with a successful login message.

There is a test user in the database and the passowrd is MD5(fantastic), this should be taken in to account when validating the user.

If a user is not logged in and tries to access the update.php page they should be redirected to index.php with an error message displayed.

When a user is logged in the navigation should change to reflect this. There should be the option to logout, when clicked the customer will be cleared from the session and redirected back to the homepage with a success message.

## Step 3 - Dynamic Content  ##
On page load show the first two tweets from *./json/tweets.json* within the relevant space on the designs, this should be rendered out without the use of javascript. Then after 5 seconds refresh the Twitter box with the last two tweets from the same file by using an ajax request. Ideally the ajax response should be the output for the tweets in a json format rather than markup being done within the javascript file.

## Step 4 - Pricing Matrix  ##
When a user is logged in find the relevant discount from the *user_discount* table using the user_id to link the tables. Update the price shown in the fourth box to be the original price minus the percentage discount from *user_discount*.

## Setup ##
In order to complete the test you will need to setup a local development environment (WAMP, MAMP, XAMP etc). You will need to clone this repository to your local setup.

http://www.wampserver.com/  
https://www.mamp.info/en/  
https://www.apachefriends.org/index.html

#### Database ####
In *./sql/* you will find database.sql. Importing this will create two tables (user, user_pricing) with test data already available. You will need to import this to your environment to manipulate the data. The password for the test user is MD5(fantastic), this will be needed for validation.

#### Site ####
All your site functionality, files and assets should go within *./site/*. An example structure would be:

* assets
* controllers
* models
* views

#### Artwork ####
All artwork is located in *./artwork/*. Within the application there should be two pages, 'Home' and 'Update' both of which have original PSD's and PNG's for quick view. The application has been designed to use the bootstrap grid system.

## Submission ##
Once you have completed the technical test create your own public repository and forward the clone address to m.thorp@fantasticmedia.co.uk along with any notes or comments you think are relevant.

### Contact ###
Miles Thorp  
**Email** m.thorp@fantasticmedia.co.uk